### networkx_update patch references

____________________________________

- [AttributeError: 'DiGraph' object has no attribute 'edge'](https://github.com/DerwenAI/pytextrank/issues/10)
- [dict_keyiterator' has no len()](https://stackoverflow.com/questions/53584341/len-throws-with-dict-keyiterator-has-no-len-when-calculating-outgoing-and-in)
- [TypeError: 'dict_keyiterator' object is not subscriptable ](https://stackoverflow.com/questions/60331374/typeerror-dict-keyiterator-object-is-not-subscriptable-python-2)
- [RuntimeError: dictionary changed size during iteration](https://stackoverflow.com/questions/11941817/how-can-i-avoid-runtimeerror-dictionary-changed-size-during-iteration-error)

