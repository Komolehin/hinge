## Hinge Autopkgtest

- The test breaks at line 55 of `run-demo` due to `networkx` with the following error:

**Traceback (most recent call last):**
  **File "/usr/bin/../lib/hinge/pruning_and_clipping.py", line 1407, in <module>**
    **Gs = random_condensation_sym(G1,1000)**
         
  **File "/usr/bin/../lib/hinge/pruning_and_clipping.py", line 452, in random_condensation_sym**
    **node = g.nodes()[random.randrange(len(g.nodes()))]**
           
  **File "/usr/lib/python3/dist-packages/networkx/classes/reportviews.py", line 194, in __getitem__**
    **return self._nodes[n]**
           
**KeyError: 819**